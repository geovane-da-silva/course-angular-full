describe('ventana principal', ()=>{
    it('tiene encabezado correcto y en espanyol por defecto', ()=>{
        cy.visit('http://localhost:4200');
        cy.contains('Discográfica');
        cy.get('h1 b').should('contain', 'HOLA es')
    });
    it('tiene boton para adicionar cancion', () => {        
        cy.get('[id="btn-add-song"]').should('have.class', 'btn btn-success')
    });
    it('tiene un place holder', ()=>{
        cy.get('[id="titulo"]')
        .invoke('css', 'placeholder')
        .should('equal', 'Título de la Canción')
    });    
});