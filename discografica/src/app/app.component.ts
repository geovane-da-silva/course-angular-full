import { Component } from '@angular/core';
import { Observable, from } from 'rxjs';
import { AuthService } from './services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {  
  constructor(private authService: AuthService, private translate: TranslateService){
    console.log('******** get translation');
    translate.getTranslation('en').subscribe(x => console.log('x: '+JSON.stringify(x)));
    translate.setDefaultLang('es');
  }
  title = 'Discográfica';
  time = new Observable(observer => {
    setInterval(() => {observer.next(new Date().toString())},1000);
  });
  
}
