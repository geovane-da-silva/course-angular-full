import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule,Routes } from '@angular/router';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import Dexie from 'dexie';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { AppComponent } from './app.component';
import { CacionComponent } from './components/cancion/cancion.component';
import { AlbumComponent } from './components/album/album.component';
import { FavoritoComponent } from './components/favorito/favorito.component';
import { CancionState, VotosState, ReducerCancion, initializaCancionState, CancionEffects, InitMyDataAction, ReducerVoto, initializaVotosState } from './models/canciones-state.model';
import { FormCancionComponent } from './components/forms/form-cancion/form-cancion.component';
import { CancionesApiClient } from './models/cancion-api-client.model';
import { BienvenidoComponent } from './components/bienvenido/bienvenido.component';
import { LoginComponent } from './components/login/login/login.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { AutorComponent } from './components/autor/autor.component';
import { AlbumPortadaComponent } from './components/album/album-portada/album-portada.component';
import { AlbumDescripcionComponent } from './components/album/album-descripcion/album-descripcion.component';
import { DescargasModule } from './descargas/descargas.module';
import { CancionDetalleComponent } from './components/cancion/cancion-detalle/cancion-detalle.component';
import { FormAutorComponent } from './components/forms/form-autor/form-autor.component';
//import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

import { Cancion } from './models/cancion';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { DescargasInterrumpidasComponent } from './descargas/descargas-interrumpidas/descargas-interrumpidas.component';
import { DescargasListadoComponent } from './descargas/descargas-listado/descargas-listado.component';
import { routesChildDescasrga } from './descargas/descargas-routing.module';
import { CancionDelMesComponent } from './components/cancion/cancion-mes/cancion-mes';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';


//init routing
export const childrenRoutsAlbum: Routes = [
  {path : '', redirectTo : 'portada', pathMatch : 'full'},
  {path : 'portada', component : AlbumPortadaComponent},
  {path : 'descripcion', component : AlbumDescripcionComponent},
  {path: 'detallecancion', component: CancionDetalleComponent}
  
];

const routes :Routes = [
  {path : '', redirectTo : 'home', pathMatch : 'full'},  
  {path : 'home', component : BienvenidoComponent},
  {path : 'album', component : AlbumComponent, children: childrenRoutsAlbum},
  {path : 'login', component : LoginComponent},
  {path : 'autor', component : AutorComponent, canActivate: [UsuarioLogueadoGuard]}, 
  {path : 'descarga', component : DescargasListadoComponent,
   canActivate: [UsuarioLogueadoGuard],children: routesChildDescasrga}, 
  {path : '**', redirectTo : 'home', pathMatch : 'full'}
];
//fin routing

//app config init

export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

//fin app config

//redux init
export interface AppState{
  cancion: CancionState;
  votos: VotosState;
}

const reducers: ActionReducerMap<AppState> = {
  cancion: ReducerCancion,
  votos: ReducerVoto
};

let reducersInitializeState = {
  cancion: initializaCancionState(),
  votos: initializaVotosState()
};
//redux fin init

//app init

export function initApp(appLoadService: AppLoadService): () => Promise<any>{
  return ()=>appLoadService.initializeCancionesState();
}
@Injectable()
class AppLoadService{
  async initializeCancionesState(): Promise<any> {
     const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
     const req = new HttpRequest('GET',APP_CONFIG_VALUE.apiEndpoint + '/my', {headers: headers});
     const res: any = await this.http.request(req).toPromise();
     this.store.dispatch( new InitMyDataAction(res.body));
  }
  constructor(private store: Store<AppState>, private http: HttpClient){}  
}

//fin app init

//dexie db
export class Translation{
  constructor(public id: number, public lang: string, public key: string, public value: string){}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  canciones: Dexie.Table<Cancion, number>;
  translations: Dexie.Table<Translation, number>;
  constructor () {
      super('MyDatabase');
      this.version(1).stores({
        canciones: 'titulo,genero,autor'
      });
      this.version(2).stores({
        canciones: 'titulo,genero,autor',
        translations: 'titulo,lang,key,value'
      });
  }
}

export const db = new MyDatabase();
//fin dexie db

// i18n ini
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                                        if (results.length === 0) {
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.translations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        return results;
                                      }).then((traducciones) => {
                                        console.log('traducciones cargadas:');
                                        console.log(traducciones);
                                        return traducciones;
                                      }).then((traducciones) => {
                                        return traducciones.map((t) => ({ [t.key]: t.value}));
                                      });
    /*
    return from(promise).pipe(
      map((traducciones) => traducciones.map((t) => { [t.key]: t.value}))
    );
    */
   return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    CacionComponent,
    AlbumComponent,
    FavoritoComponent,
    FormCancionComponent,
    BienvenidoComponent,
    LoginComponent,
    AutorComponent,
    AlbumPortadaComponent,
    AlbumDescripcionComponent,
    CancionDetalleComponent,
    FormAutorComponent,
    DescargasInterrumpidasComponent,
    CancionDelMesComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    HttpClientModule,
    NgRxStoreModule.forRoot(reducers,{initialState: reducersInitializeState}),
    EffectsModule.forRoot([CancionEffects]),
    StoreDevtoolsModule.instrument(),
    DescargasModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule
    //NgxMapboxGLModule
  ],
  providers: [CancionesApiClient, AuthService, UsuarioLogueadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    {provide: APP_INITIALIZER, useFactory: initApp, deps: [AppLoadService], multi: true},
    MyDatabase
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
