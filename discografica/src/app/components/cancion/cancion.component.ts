import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Cancion } from '../../models/cancion';
import {trigger, state, style, animate, transition} from '@angular/animations';
import { from } from 'rxjs';

@Component({
  selector: 'app-cancion',
  templateUrl: './cancion.component.html',
  styleUrls: ['./cancion.component.css'],
  animations: [
    trigger('esFavorita', [
      state('estadoFavorita', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorita', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorita => estadoFavorita', [
        animate('3s')
      ]),
      transition('estadoFavorita => estadoNoFavorita', [
        animate('1s')
      ])
    ])
  ]
})
export class CacionComponent implements OnInit {  
  @Input() cancion :Cancion;
  @Input('numero') numeroPista :number;
  @HostBinding('attr.class') cssClass='col-md-6';
  @Output() notificacionFavorito :EventEmitter<Cancion>;
  @Output() notificacionVotoMas :EventEmitter<Cancion>;
  @Output() notificacionVotoMenos :EventEmitter<Cancion>;
  

  constructor() {    
    this.notificacionFavorito = new EventEmitter<Cancion>();
    this.notificacionVotoMas = new EventEmitter<Cancion>();
    this.notificacionVotoMenos = new EventEmitter<Cancion>();
  }

  favorito(c :Cancion){    
    this.notificacionFavorito.emit(c);
  }

  votoMas(c: Cancion){
    this.notificacionVotoMas.emit(c);
  }
  votoMenos(c: Cancion){
    this.notificacionVotoMenos.emit(c);
  }

  ngOnInit() {
  }

}
