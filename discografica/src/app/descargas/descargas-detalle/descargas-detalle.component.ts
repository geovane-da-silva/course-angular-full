import { Component, OnInit } from '@angular/core';
import { DescargasApiClientService } from '../descargas-api-client.service';
import { DescargaInterrumpidaApi } from '../descargas-interrumpidas-api-client';

@Component({
  selector: 'app-descargas-detalle',
  templateUrl: './descargas-detalle.component.html',
  styleUrls: ['./descargas-detalle.component.css']
  
})
export class DescargasDetalleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
