import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DescargasListadoComponent } from './descargas-listado/descargas-listado.component';
import { DescargasDetalleComponent } from './descargas-detalle/descargas-detalle.component';
import { DescargasInterrumpidasComponent } from './descargas-interrumpidas/descargas-interrumpidas.component';

export const routesChildDescasrga: Routes = [ 
  {path: 'listaDescargas', component: DescargasListadoComponent},
  {path: 'detalleDescarga', component: DescargasDetalleComponent},
  {path: 'interrumpidas', component: DescargasInterrumpidasComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routesChildDescasrga)],
  exports: [RouterModule]
})
export class DescargasRoutingModule { }
