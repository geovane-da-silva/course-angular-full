import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DescargasRoutingModule } from './descargas-routing.module';
import { DescargasListadoComponent } from './descargas-listado/descargas-listado.component';
import { DescargasDetalleComponent } from './descargas-detalle/descargas-detalle.component';
import { DescargasApiClientService } from './descargas-api-client.service';

@NgModule({
  declarations: [DescargasListadoComponent, DescargasDetalleComponent],
  imports: [
    CommonModule,
    DescargasRoutingModule
  ],
  providers:[DescargasApiClientService]
})
export class DescargasModule { }
