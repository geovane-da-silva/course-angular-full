import { Injectable } from "@angular/core";
import { CancionesApiClient } from './cancion-api-client.model';
import { Cancion } from './cancion';

@Injectable()
export class CancionDelMesApi extends CancionesApiClient{
    getCancionDelMes():Cancion{
        return new Cancion('cancion del mes','Rock','Jorge');
    }
}