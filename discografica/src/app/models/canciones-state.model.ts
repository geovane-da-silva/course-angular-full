import { Cancion } from './cancion';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface CancionState{
    items: Cancion[];
    favoritos: Cancion[];
}

export interface VotosState{
    votosPositivos: number;
    votosNegativos: number;
}

export function initializaCancionState(){
    return {items: [],favoritos: []};
}
export function initializaVotosState(){
    return {votosPositivos:0,votosNegativos:0};
}

export enum CancionActionType{
    NUEVA_CANCION = '[Cancion] Nueva',
    NUEVO_FAVORITA = '[Cancion] Favorita',
    NUEVA_MAS_RECIENTE = '[Cancion] Reciente',
    ELIMINA_FAVORITA = '[Cancion] Elimina Favorita',
    VOTO_MAS = '[Cancion] Voto Mas', 
    VOTO_MENOS = '[Cancion] Voto Menos',
    INIT_MY_DATA = '[Cancion] init my data',
    DESCARGAR_CANCION = '[Cancion] descargar'
}

enum VotosActionType{
    VOTO_POSITIVO='[Votos] positivo',VOTO_NEGATIVO='[Votos] negativo',INIT_VOTOS='[Votos] init'
}

export class VotoPositivoAction implements Action{
    type = VotosActionType.VOTO_POSITIVO;
}
export class VotoNegativoAction implements Action{
    type = VotosActionType.VOTO_NEGATIVO;
}
export class InitVotosAction implements Action{
    type = VotosActionType.INIT_VOTOS;
}

export class NuevaCancionAction implements Action{
    type = CancionActionType.NUEVA_CANCION;
    constructor(public cancion: Cancion){}
}

export class NuevaFavoritoAction implements Action{  
    type;
    constructor(public cancion: Cancion){
        if(cancion.favorita){
            this.type = CancionActionType.ELIMINA_FAVORITA;
        }else{
            this.type = CancionActionType.NUEVO_FAVORITA;
        }
    }
}

export class ElegidoMasRecienteAction implements Action{
    type = CancionActionType.NUEVA_MAS_RECIENTE;
    constructor(public cancion: Cancion){}
}

export class VotoMasAction implements Action{
    type = CancionActionType.VOTO_MAS;
    constructor(public cancion: Cancion){}
}

export class VotoMenosAction implements Action{
    type = CancionActionType.VOTO_MENOS;
    constructor(public cancion: Cancion){}
}

export class InitMyDataAction implements Action{
    type = CancionActionType.INIT_MY_DATA;
    constructor(public canciones: Cancion[]){}
}

export class DescargarAction implements Action{
    type = CancionActionType.DESCARGAR_CANCION;
    constructor(public cancion: Cancion){}
}

export type VotoAction = InitVotosAction | VotoPositivoAction | VotoNegativoAction;

export type CancionAction = NuevaCancionAction | NuevaFavoritoAction | 
            ElegidoMasRecienteAction | VotoMasAction | VotoMenosAction | 
            InitMyDataAction | DescargarAction;

export function ReducerCancion(state: CancionState, action: CancionAction)
:CancionState{
    switch (action.type) {
        case CancionActionType.INIT_MY_DATA:{
            const canciones: Cancion[] = (action as InitMyDataAction).canciones;
            return {...state, items: canciones};
        }
        case CancionActionType.NUEVA_CANCION:{
            return {...state, items: [...state.items, (action as NuevaCancionAction).cancion]};
        }
        case CancionActionType.NUEVO_FAVORITA:{
            let favorita: Cancion = (action as NuevaFavoritoAction).cancion;
            favorita.favorita = true;
            return {...state, favoritos: [...state.favoritos, (action as NuevaFavoritoAction).cancion]};
        }
        case CancionActionType.ELIMINA_FAVORITA:{
            let favorita: Cancion = (action as NuevaFavoritoAction).cancion;            
            favorita.favorita = false;                           
            return {...state, favoritos: state.favoritos.filter(f => f.favorita)};
        }
        case CancionActionType.NUEVA_MAS_RECIENTE:
        {
            state.items.forEach(x => x.mas_reciente = false);
            let reciente: Cancion = (action as ElegidoMasRecienteAction).cancion;
            reciente.mas_reciente = true;
            return{ ...state };
        }  
        case CancionActionType.VOTO_MAS:{
            let c: Cancion = (action as VotoMasAction).cancion;            
            c.votos ++;            
            return {...state};
        }        
        case CancionActionType.VOTO_MENOS:{
            let c: Cancion = (action as VotoMenosAction).cancion;            
            c.votos --;            
            return {...state};
        }   
    }
    return state;
}

export function ReducerVoto(state: VotosState, action: VotoAction): VotosState{
   switch (action.type) {
       case VotosActionType.INIT_VOTOS:{
            return {...state, votosPositivos: 0, votosNegativos:0};
       }
       case VotosActionType.VOTO_POSITIVO:{
           const votosPos = state.votosPositivos;
           const votosNeg = state.votosNegativos;
           return {...state,votosPositivos:votosPos+1,votosNegativos:votosNeg};
        }
        case VotosActionType.VOTO_NEGATIVO:{
            const votosPos = state.votosPositivos;
            const votosNeg = state.votosNegativos;
            return {...state,votosPositivos:votosPos,votosNegativos:votosNeg+1};
         }
   }
   return state;
}

@Injectable()
export class CancionEffects{
    @Effect()
    NuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(CancionActionType.NUEVA_CANCION),
        map((action: NuevaCancionAction)=>new ElegidoMasRecienteAction(action.cancion))
    );
    constructor(private actions$: Actions){}
}