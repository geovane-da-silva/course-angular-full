import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/forms/form-destino-viaje/form-destino-viaje.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { DestinosViajesSate, ReducerDestinosViajes, InitializeDestinoViajeState, DestinosViajesEffects } from './models/destinos-viajes-state.models';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';

//app config

export interface AppConfig{
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'http://localhost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

// fin app config

export const childrenRoutesVuelos: Routes = [
  {path : '', redirectTo : 'main', pathMatch : 'full'},
  {path : 'main', component : VuelosMainComponent},
  {path : 'mas-info', component : VuelosMasInfoComponent},
  {path : ':id', component : VuelosDetalleComponent},
]

const routes :Routes = [
  {path : '', redirectTo : 'home', pathMatch : 'full'},
  {path : 'home', component : ListaDestinosComponent},
  {path : 'destino/:id', component : DestinoDetalleComponent},
  {path : 'login', component : LoginComponent},
  {path : 'protected', component : ProtectedComponent, canActivate: [UsuarioLogueadoGuard]},
  {path : 'vuelos', component : VuelosComponent, 
  canActivate: [UsuarioLogueadoGuard], children: childrenRoutesVuelos}
]

//redux init
export interface AppState{
  destinos: DestinosViajesSate;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: ReducerDestinosViajes
};

let reducersInitializeState = {
  destinos: InitializeDestinoViajeState()
};
//redux fin init


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule, 
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers,{initialState: reducersInitializeState}),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule
  ],
  providers: [DestinosApiClient, AuthService, UsuarioLogueadoGuard,
  { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE }
  ],  
  
  bootstrap: [AppComponent]
})
export class AppModule { }
