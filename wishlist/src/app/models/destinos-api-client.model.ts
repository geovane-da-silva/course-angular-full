import { DestinoViaje } from "./destino-viaje.model"
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.models';
import { Injectable, inject, forwardRef } from '@angular/core';

@Injectable()
export class DestinosApiClient{    
    constructor(private store: Store<AppState>){ 
    }
    add(destino :DestinoViaje){
        this.store.dispatch(new NuevoDestinoAction(destino));    
    }    
    elegir(destino :DestinoViaje){
        this.store.dispatch(new ElegidoFavoritoAction(destino));
    }   
    getById(id: string){
        return new DestinoViaje("nuevo","");
    } 
}